const {PubSub} = require("@google-cloud/pubsub");
const cors = require ('cors')
const express = require ('express')
const {LoggingBunyan} = require('@google-cloud/logging-bunyan');
const bunyan = require('bunyan')

const app = express();
app.use(cors())
app.use(express.json())
const pubsub = new PubSub({projectId:"revassess-dev"});
const cloudLogger = new LoggingBunyan();

const config = {
    name:'revassess-dev', 
    //streams are where you log the data
    streams:[
        {stream:process.stdout, level:'info'},
        cloudLogger.stream('info')
    ]
}
const logger = bunyan.createLogger(config)

app.post('/assessment', async (req, res) =>{

    if(!email || !exerciseId || !tests || !sourceCode){
        logger.error("Invalid assessment submitted. Missing required fields.")
        res.status(400).send("Missing required fields.")
    }
    try{
        let assessment = req.body;
        const result = await axios.get(`http://someaddressthatsomeonesgonnagiveme/exerciseId/verify/${assessment.exerciseId}`); // get correct file path name
        if(!result.data){
            logger.error(`The exercise with Id ${assessment.exerciseId} could not be found`);
            res.status(404).send(`The exercise with Id ${assessment.exerciseId} could not be found`);
        }
        assessment.assessmentTime = Date.now();
    } catch {
        logger.error(`There was an issue verifying the exercise with Id ${assessment.exerciseId}`);
        res.status(403).send(`There was an issue verifying the exercise with Id ${assessment.exerciseId}`);
    }
    const response = await pubsub.topic('revassess-topic').publishJSON(assessment); 
    logger.info("Assessment submitted successfully.");
    res.status(201).send("Assessment submitted successfully.");
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, ()=>{console.log(`Assessment ingestion started on port ${PORT}`)});