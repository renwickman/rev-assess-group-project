import { Box, Grid, Paper, TextField, Button, Stack } from '@mui/material';
import axios from 'axios';
import { SyntheticEvent, useRef, useState } from 'react'

export default function LoginPage(){

    const email = useRef<HTMLInputElement>(null);
    const password= useRef<HTMLInputElement>(null);

    const [token, setToken] = useState('');

    async function login(event:SyntheticEvent){
        try{
            const response = await axios.patch('/users/login');
            setToken(response.data["token"]);
            alert('Login Successful');
        } catch (err) {
            alert('Bad Login');
        }
    }

    return(<Box><Paper>
        <Stack spacing={1}>
            <TextField variant="standard" label="Email" inputRef={email} />
            <TextField variant="standard" label="Password" inputRef={password}/>
            <Button variant="contained" color="primary" onClick={login}>Submit</Button>
        </Stack>
    </Paper></Box>)
}
