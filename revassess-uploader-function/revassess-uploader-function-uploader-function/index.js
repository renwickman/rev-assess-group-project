const { Storage } = require('@google-cloud/storage');
const storage = new Storage();
// const cors = require('cors')();

exports.helloPubSub = async (event, context) => {
    const message = Buffer.from(event.data, 'base64').toString();
    const assessment = JSON.parse(message);
    console.log(assessment.email);
    const bucket = storage.bucket('revassess-code-bucket');
    const buffer = Buffer.from(assessment.sourceCode, "base64");
    const file = bucket.file('test.zip');
    await file.save(buffer);
    await file.makePublic();
    console.log(file.publicUrl());
};


// const { Storage } = require("@google-cloud/storage");
// const storage = new Storage();
// const cors = require('cors')();

// exports.upload = async (req, res) => {
// cors (req,res, async () => {
// const body = req.body;
// const bucket = storage.bucket("baeder-expense-attachments");
// const buffer = Buffer.from(body.content, "base64");
// const file = bucket.file(`${body.fileName}`);
// await file.save(buffer);
// await file.makePublic();
// res.send({ photoLink: file.publicUrl() });
// })